const express = require('express'),
  path = require('path');

// Export
module.exports = function () {
  let server = express(),
    create,
    start;

  create = function (config) {
    // Settings
    server.set('env', process.env.NODE_ENV || config.env);
    server.set('port', process.env.PORT || config.port);
    server.set('hostname', config.hostname);

    // Static
    server.use(express.static(path.join(__dirname, config.staticDir)));
    //server.use(express.static(path.join(__dirname, '../node_modules')));

    // Routes
    // server.use('/', function (req, res) {
    //   res.sendFile(path.join(__dirname, config.indexHtml));
    // });
    // server.get('/', (req, res) => {
    //   res.sendFile(path.join(__dirname, config.indexHtml));
    // });
  };

  start = function () {
    let hostname = server.get('hostname'),
      port = server.get('port');

    return server.listen(port, function () {
      console.log(`SERVER: listening on http://${hostname}:${port}`);
    });
  };

  return {
    create,
    start,
  };
};
