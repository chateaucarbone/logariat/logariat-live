const ioHook = require('iohook');

module.exports = function (comm) {
  ioHook.on('keydown', (event) => {
    comm.send('key', event.keycode);
  });

  // Register and start hook
  ioHook.start(); // true: debug
};
