const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

module.exports = function (config, comm) {
  const serial = new SerialPort(config.port, {
    baudRate: config.baudrate,
  });

  // Parser (Buffer to number)
  const parser = new Readline();
  serial.pipe(parser);

  // Opening errors
  serial.on('error', function (err) {
    console.log('HEART/ERROR:', err.message);
  });

  parser.on('data', function (value) {
    comm.send('heart', value);
    //console.log('HEART:', value);
  });
};
