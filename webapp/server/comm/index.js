// Websockets & OSC
const socketsComm = require('./sockets');
const oscComm = require('./osc');

module.exports = function () {
  let io, osc;

  const create = function (config, httpServer) {
    io = socketsComm();
    io.create(httpServer);
    osc = oscComm();
    osc.create(config.osc);
  };

  const start = function () {
    io.start();
    osc.start();
  };

  const send = function (name, value) {
    io.send(name, value);
    osc.send(name, value);
  };

  return {
    create,
    start,
    send,
  };
};

// module.exports = router;
// exports.alerts = (req, res, next) => {
