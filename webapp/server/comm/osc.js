const osc = require('osc');

// Export
module.exports = function () {
  let oscServer, config;

  const create = function (conf) {
    config = conf;
    oscServer = new osc.UDPPort({
      localAddress: config.address,
      localPort: config.port,
    });
  };

  const start = function () {
    oscServer.on('ready', function () {
      console.log(`OSC: listening on ${config.address}:${config.port}`);
    });

    oscServer.open();
    //return oscServer;
  };

  const send = function (name, value) {
    oscServer.send(
      {
        address: `/${name}`,
        args: [
          {
            type: 'i',
            value: value,
          },
        ],
      },
      config.address,
      config.sendport,
    );
  };

  return {
    create,
    start,
    send,
  };
};

// oscServer.on('message', function (oscMessage) {
// 		if (oscMessage.address === '/key'){
//       //console.log('python key osc');
//       io.emit('keyboard', oscMessage.args[0])
//     }
// 		//else if (oscMessage.address === '/heart/raw') console.log('HEART:', oscMessage.args[0])
// 		//else if (oscMessage.address === '/render/shapes') io.emit('renderShapes', oscMessage.args[0])
// });

// const oscServer2 = new osc.UDPPort({
//   localAddress: '127.0.0.1',
//   localPort: 9001,
// });

// oscServer2.on('ready', function () {
//   console.log('Listening for OSC over UDP. 9001');
// });

// oscServer2.on('message', function (oscMessage) {
//   if (oscMessage.address === '/shapes') io.emit('shapes', oscMessage.args[0]);
// });

// oscServer2.open();
