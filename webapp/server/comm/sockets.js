const socketio = require('socket.io');

module.exports = function () {
  let io;

  const create = function (server) {
    io = socketio.listen(server);
  };

  const start = function () {
    io.sockets.on('connection', function (socket) {
      console.log('SOCKET: client connected');
      socket.on('hello', function (data) {
        console.log('SOCKET: receive', data);
      });
    });
    // return io;
  };

  const send = function (name, value) {
    io.emit(name, value);
  };

  return {
    create,
    start,
    send,
  };
};
