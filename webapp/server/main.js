// Settings file
const config = require('./config.json');

// Start HTTP server
const server = require('./server')();
server.create(config.server);
const httpServer = server.start();

// Communications : websockets, osc
const comm = require('./comm')();
comm.create(config, httpServer);
comm.start();

// Sensors
require('./sensors/keyboard')(comm);
require('./sensors/heart')(config.serial, comm);
// Hand
