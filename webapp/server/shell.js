//////////////////////// shell
const pty = require('node-pty')

const ptyProcess = pty.spawn('bash', [], {
  name: 'xterm-color',
  cwd: path.join(__dirname, '../home'),
  env: process.env
})

ptyProcess.on('data', (data) => {
  //process.stdout.write(data)
  //console.log('pty > xterm', data);
  io.emit('fromPty', data) // pty > xterm
})

process.on('exit', () => ptyProcess.kill());


