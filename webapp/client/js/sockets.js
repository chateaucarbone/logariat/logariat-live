const socket = io();
//io.connect('http://localhost:3000');

let data = [];

function init() {
  socket.on('connect', function () {
    socket.emit('hello', 'Hello World from client');

    socket.on('key', (keycode) => {
      console.log(`SOCKET / key: ${keycode}`);
      data[0] = keycode;
    });

    socket.on('heart', (value) => {
      //console.log(`SOCKET / heart: ${value}`);
      data[1] = value;
    });
  });

  //return socket;
}

function getData() {
  return data;
}

export { init, getData };
