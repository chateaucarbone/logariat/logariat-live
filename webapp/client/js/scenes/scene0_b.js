import { LineMaterial } from '../libs/lines/LineMaterial.js';
import { LineGeometry } from '../libs/lines/LineGeometry.js';
import { Line2 } from '../libs/lines/Line2.js';
import { GeometryUtils } from '../libs/utils/GeometryUtils.js';

let elements, canvas, width, height, scene, renderer, camera, mesh, stats, cube;
let matLine;
const speed = 0.01;

function init(el) {
  elements = el;
  canvas = elements.z0;
  width = canvas.clientWidth; // window.innerWidth;
  height = canvas.clientHeight;

  initDOM();
  initScene();
  // initLights();
  initLines();
  initCube();
  initCamera();
  initRenderer();

  render();
}

function initDOM() {
  canvas.classList.remove('hide');
  elements.main.classList.add('hide');
}

function initScene() {
  const clock = new THREE.Clock();
  stats = new Stats();
  stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
  document.body.appendChild(stats.dom);
  scene = new THREE.Scene();
  scene.background = new THREE.Color('#888');
}

function initCamera() {
  //fieldOfView, aspectRatio, near clipping plane, far clipping plane
  // camera = new THREE.OrthographicCamera(
  //   width / -2,
  //   width / 2,
  //   height / 2,
  //   height / -2,
  //   1,
  //   1000,
  // );

  // camera = new THREE.PerspectiveCamera(40, width / height, 10, 100);
  // camera.position.set(0, 0, 100);
  // camera.lookAt(scene.position);

  camera = new THREE.PerspectiveCamera(
    40,
    window.innerWidth / window.innerHeight,
    1,
    1000,
  );
  //camera.position.set(0, 0, 50);
  camera.position.set(0, 0, 100);

  camera.lookAt(scene.position);

  //camera = new THREE.PerspectiveCamera(70, width / height, 1, 10); //35, 0.1, 100
  //camera.position.set(0, 3.5, 5); //  (-4, 4, 10);
  //camera.lookAt(scene.position);
}

function initRenderer() {
  renderer = new THREE.WebGLRenderer({
    canvas: canvas, //?
    antialias: true,
    alpha: true, //?
  });
  renderer.setSize(width, height);
  renderer.setPixelRatio(window.devicePixelRatio);

  // const DPR = window.devicePixelRatio ? window.devicePixelRatio : 1;
  // renderer.setPixelRatio(DPR); // ou  window.devicePixelRatio
  // renderer.gammaFactor = 2.2;
  // renderer.physicallyCorrectLights = true;

  document.body.appendChild(renderer.domElement); // ?canvas.appendChild(
}

function initLights() {
  const ambientLight = new THREE.Hemscene.add(line);
  isphereLight(
    0xdf00ee, // sky color
    0x202020, // ground color
    5, // intensity
  );
  const mainLight = new THREE.DirectionalLight(0xff0000, 5);
  mainLight.position.set(10, 10, 10);
  scene.add(ambientLight, mainLight);
}

function initLines() {
  var positions = [];
  for (var i = 0; i < 100; i++) {
    positions.push(i, Math.random(100), 0);
  }

  var geometry = new LineGeometry();
  geometry.setPositions(positions);

  matLine = new LineMaterial({
    color: 0xffffff,
    linewidth: 5, // in pixels
    vertexColors: false,
    //resolution:  // to be set by renderer, eventually
    dashed: false,
  });

  var line = new Line2(geometry, matLine);
  line.computeLineDistances();
  line.scale.set(5, 10, 1);
  scene.add(line);

  // var geometry = new LineGeometry();
  // geometry.setPositions(positions);
  // // geometry.setColors(colors);

  // var matLine = new LineMaterial({
  //   color: 0xffffff,
  //   linewidth: 20, // in pixels
  //   //vertexColors: true,
  //   //resolution:  // to be set by renderer, eventually
  //   dashed: false,
  // });

  // var line = new Line2(geometry, matLine);
  // //line.computeLineDistances();
  // line.scale.set(1, 1, 1);
  // scene.add(line);

  // var points = [];
  // points.push(new THREE.Vector3(-10, 0, 0));
  // points.push(new THREE.Vector3(0, 10, 0));
  // points.push(new THREE.Vector3(10, 0, 0));

  // const material = new THREE.LineBasicMaterial({
  //   color: 0x0000ff,
  //   linewidth: 5,
  // });

  // var geometry = new THREE.BufferGeometry().setFromPoints(points);
  // var line = new THREE.Line(geometry, material);
  // scene.add(line);

  // var geometry = new THREE.Geometry();
  // for (var j = 0; j < Math.PI; j += (2 * Math.PI) / 100) {
  //   var v = new THREE.Vector3(Math.cos(j), Math.sin(j), 0);
  //   geometry.vertices.push(v);
  // }
  // var line = new MeshLine();
  // line.setGeometry(geometry, function (p) {
  //   return 4;
  // }); // makes width 4 * lineWidth

  // var material = new MeshLineMaterial({
  //   useMap: false,
  //   color: new THREE.Color('0x00ff0'),
  //   opacity: 1,
  //   resolution: resolution,
  //   sizeAttenuation: !false,
  //   lineWidth: 4,
  //   near: camera.near,
  //   far: camera.far,
  // });
  // var mesh = new THREE.Mesh(line.geometry, material); // this syntax could definitely be improved!
  // scene.add(mesh);

  // var geometry = new LineGeometry();
  // geometry.setPositions(points);

  // var matLine = new LineMaterial({
  //   color: 0xffffff,
  //   linewidth: 5, // in pixels
  //   //resolution:  // to be set by renderer, eventually
  // });

  // line = new Line2(geometry, matLine);
  // line.computeLineDistances();
  // line.scale.set(1, 1, 1);
  // scene.add(line);
}

function initCube() {
  cube = new THREE.Mesh(
    new THREE.CubeGeometry(2, 2, 2),
    new THREE.MeshNormalMaterial(),
  );
  scene.add(cube);
}

function rotateCube() {
  cube.rotation.x -= speed * 2;
  cube.rotation.y -= speed;
  cube.rotation.z -= speed * 3;
}

// ! heavy computation
function update() {
  rotateCube();
  matLine.resolution.set(window.innerWidth, window.innerHeight); // resolution of the viewport
  stats.update();
}

function render() {
  requestAnimationFrame(render);
  update();
  //renderer.setClearColor(0x000000, 0);
  //renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);
  // renderer will set this eventually
  renderer.render(scene, camera);
}

// ! heavy computation
function onWindowResize() {
  // set the aspect ratio to match the new browser window aspect ratio
  camera.aspect = window.innerWidth / window.innerHeight;
  // update the camera's frustum
  camera.updateProjectionMatrix();
  // update the size of the renderer AND the canvas
  renderer.setSize(window.innerWidth, window.innerHeight);
}

window.addEventListener('resize', onWindowResize);

export { init };
