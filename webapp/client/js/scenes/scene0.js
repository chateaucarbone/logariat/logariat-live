let elements, canvas, width, height, scene, renderer, camera, mesh, stats, cube;
let positions;
let line;
var MAX_POINTS = 130;
let socket;

function init(el, sock) {
  elements = el;
  socket = sock;
  canvas = elements.z0;
  width = canvas.clientWidth; // window.innerWidth;
  height = canvas.clientHeight;

  initSocket();
  initDOM(); // remove/add .hide class
  initScene();
  initLights();
  initLines();
  initCamera();
  initRenderer();

  render();
}

function initSocket() {
  //socket.on('connect', function () {
  socket.emit('hello', 'Hello from client scene 0');
  socket.on('heart', (keycode) => {
    //console.log(`Socket receive - heart: ${keycode}`);
    let i;
    for (i = 0; i < MAX_POINTS - 1; i++) {
      positions[i * 3 + 1] = positions[i * 3 + 4];
    }
    positions[MAX_POINTS * 3 - 2] = (keycode / 1024) * 20;
    line.geometry.attributes.position.needsUpdate = true;
  });
}

function initDOM() {
  canvas.classList.remove('hide');
  elements.main.classList.add('hide');
}

function initScene() {
  stats = new Stats();
  stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
  document.body.appendChild(stats.dom);
  scene = new THREE.Scene();
  scene.background = new THREE.Color('#000');
}

function initCamera() {
  //fieldOfView, aspectRatio, near clipping plane, far clipping plane
  camera = new THREE.PerspectiveCamera(
    40,
    window.innerWidth / window.innerHeight,
    1,
    1000,
  );
  camera.position.set(0, 0, 100);
  camera.lookAt(scene.position);
}

function initRenderer() {
  renderer = new THREE.WebGLRenderer({
    canvas, //?
    antialias: false,
    alpha: true, //?
  });
  renderer.setSize(width, height);
  renderer.setPixelRatio(window.devicePixelRatio);
  // const DPR = window.devicePixelRatio ? window.devicePixelRatio : 1;
  // renderer.setPixelRatio(DPR); // ou  window.devicePixelRatio
  // renderer.gammaFactor = 2.2;
  // renderer.physicallyCorrectLights = true;
  document.body.appendChild(renderer.domElement); // ?canvas.appendChild(
}

function initLights() {}

function initLines() {
  // geometry
  const geometry = new THREE.BufferGeometry();
  positions = new Float32Array(MAX_POINTS * 3);
  for (let i = 0; i < MAX_POINTS; i++) {
    positions[i * 3] = i;
    // positions[i * 3 + 1] = Math.random() * 10;
    positions[i * 3 + 1] = (i / MAX_POINTS) * 20;
    positions[i * 3 + 2] = 0;
  }
  geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3));

  // material
  var material = new THREE.LineBasicMaterial({
    color: 0xffffff,
    linewidth: 1,
  });

  // line
  line = new THREE.Line(geometry, material);
  const xscale = 0.5;
  line.scale.set(xscale, 1, 1);
  line.position.x = (-MAX_POINTS * xscale) / 2;
  line.position.y = -10;
  scene.add(line);
}

// ! heavy computation
function update() {
  stats.update();
  // line.geometry.attributes.position.needsUpdate = true;
}

function render() {
  requestAnimationFrame(render);
  update();
  //renderer.setClearColor(0x000000, 0);
  //renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);
  // renderer will set this eventually
  renderer.render(scene, camera);
}

// ! heavy computation
function onWindowResize() {
  // set the aspect ratio to match the new browser window aspect ratio
  camera.aspect = window.innerWidth / window.innerHeight;
  // update the camera's frustum
  camera.updateProjectionMatrix();
  // update the size of the renderer AND the canvas
  renderer.setSize(window.innerWidth, window.innerHeight);
}

window.addEventListener('resize', onWindowResize);

export { init };
