import { LineMaterial } from '../libs/lines/LineMaterial.js';
import { LineGeometry } from '../libs/lines/LineGeometry.js';
import { Line2 } from '../libs/lines/Line2.js';
import { GeometryUtils } from '../libs/utils/GeometryUtils.js';

let elements, canvas, width, height, scene, renderer, camera, mesh, stats, cube;
let matLine;
const speed = 0.01;
let data = {
  kery: 127,
  heart: 255,
};
let lineGeometry = new LineGeometry();
let line;

var MAX_POINTS = 50;
var drawCount;
let socket;

let geometry = new LineGeometry();
let positions = [];

let segments = 200;
let pos = segments;

let insetWidth, insetHeight;

//let socket;

function init(el, sock) {
  elements = el;
  socket = sock;
  canvas = elements.z0;
  width = canvas.clientWidth; // window.innerWidth;
  height = canvas.clientHeight;

  initSocket();
  initDOM(); // remove/add .hide class
  initScene();
  initLights();
  initLines();
  initCamera();
  initWebcam();
  initRenderer();

  render();
}

function initWebcam() {
  const videoElement = document.createElement('video');
  const videoTexture = new THREE.VideoTexture(videoElement);
  const videoGeometry = new THREE.PlaneBufferGeometry(100, 50);
  videoGeometry.scale(1, 1, 1);
  videoGeometry.rotateZ(0.5 * 3.14);
  videoGeometry.translate(0, 0, -1);
  const videoMaterial = new THREE.MeshBasicMaterial({ map: videoTexture });
  const videoMesh = new THREE.Mesh(videoGeometry, videoMaterial);
  //videoMesh.lookAt(camera.position);
  scene.add(videoMesh);

  if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
    const constraints = {
      video: { width: 1280, height: 720, facingMode: 'user' },
    };
    navigator.mediaDevices
      .getUserMedia(constraints)
      .then(function (stream) {
        // apply the stream to the video element used in the texture
        videoElement.srcObject = stream;
        videoElement.play();
      })
      .catch(function (error) {
        console.error('Unable to access the camera/webcam.', error);
      });
  } else {
    console.error('MediaDevices interface not available.');
  }
}

function initSocket() {
  //socket.on('connect', function () {
  socket.emit('hello', 'Hello from client scene 1');
  // socket.on('key', (val) => {
  //   //console.log(`Socket receive - key: ${val}`);
  //   let i;
  //   for (i = 0; i < segments * 3 - 3; i = i + 3) {
  //     positions.splice(i + 1, 1, positions[i + 4]);
  //   }
  //   positions.splice(segments * 3 - 2, 1, val / 127 * 20);
  //   geometry.setPositions(positions);
  // });

  socket.on('heart', (val) => {
    //console.log(`Socket receive - heart: ${val}`);
    let i;
    for (i = 0; i < segments * 3 - 3; i = i + 3) {
      positions.splice(i + 1, 1, positions[i + 4]);
    }
    positions.splice(segments * 3 - 2, 1, (val / 1023) * 20);
    geometry.setPositions(positions);
    //line.computeLineDistances();
  });
}

function initDOM() {
  canvas.classList.remove('hide');
  elements.main.classList.add('hide');
}

function initScene() {
  stats = new Stats();
  stats.showPanel(0); // 0: fps, 1: ms, 2: mb, 3+: custom
  document.body.appendChild(stats.dom);
  scene = new THREE.Scene();
  scene.background = new THREE.Color('#000');
}

function initCamera() {
  //fieldOfView, aspectRatio, near clipping plane, far clipping plane
  camera = new THREE.PerspectiveCamera(
    40,
    window.innerWidth / window.innerHeight,
    1,
    1000,
  );
  camera.position.set(0, 0, 100);
  camera.lookAt(scene.position);
}

function initRenderer() {
  renderer = new THREE.WebGLRenderer({
    canvas, //?
    antialias: true,
    alpha: true, //?
  });
  renderer.setSize(width, height);
  renderer.setPixelRatio(window.devicePixelRatio);
  // const DPR = window.devicePixelRatio ? window.devicePixelRatio : 1;
  // renderer.setPixelRatio(DPR); // ou  window.devicePixelRatio
  // renderer.gammaFactor = 2.2;
  // renderer.physicallyCorrectLights = true;
  document.body.appendChild(renderer.domElement); // ?canvas.appendChild(
}

function initLights() {}

function initLines() {
  let colors = [];
  for (let i = 0; i < segments; i++) {
    positions.push(i, Math.random() * 20, 0.0);
    let c = i / segments;
    colors.push(c, c, c);
  }

  geometry.setPositions(positions);
  geometry.setColors(colors);

  matLine = new LineMaterial({
    color: 0xffffff,
    linewidth: 2, // in pixels
    vertexColors: true,
    //resolution:  // to be set by renderer, eventually
    dashed: false,
  });

  line = new Line2(geometry, matLine);
  line.computeLineDistances();
  line.scale.set(0.5, 1, 1);
  line.position.x = -segments / 2;
  line.position.y = -10;
  scene.add(line);
}

function render() {
  requestAnimationFrame(render);
  stats.update();

  // main scene
  renderer.setClearColor(0x000000, 0);
  renderer.setViewport(0, 0, window.innerWidth, window.innerHeight);

  // renderer will set this eventually
  matLine.resolution.set(window.innerWidth, window.innerHeight); // resolution of the viewport
  renderer.render(scene, camera);

  // inset scene
  renderer.setClearColor(0x222222, 1);
  renderer.clearDepth(); // important!
  renderer.setScissorTest(true);
  renderer.setScissor(20, 20, insetWidth, insetHeight);
  renderer.setViewport(20, 20, insetWidth, insetHeight);

  // renderer will set this eventually
  matLine.resolution.set(insetWidth, insetHeight); // resolution of the inset viewport
  renderer.setScissorTest(false);
}

// ! heavy computation
function onWindowResize() {
  // set the aspect ratio to match the new browser window aspect ratio
  camera.aspect = window.innerWidth / window.innerHeight;
  // update the camera's frustum
  camera.updateProjectionMatrix();
  // update the size of the renderer AND the canvas
  renderer.setSize(window.innerWidth, window.innerHeight);

  insetWidth = window.innerHeight / 4; // square
  insetHeight = window.innerHeight / 4;
}

window.addEventListener('resize', onWindowResize);

export { init };
