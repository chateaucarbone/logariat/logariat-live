import { LineMaterial } from '../libs/lines/LineMaterial.js';
import { LineGeometry } from '../libs/lines/LineGeometry.js';
import { Line2 } from '../libs/lines/Line2.js';
import { GeometryUtils } from '../libs/utils/GeometryUtils.js';

var renderer, scene, camera;

var line;
var MAX_POINTS = 500;
var drawCount;
var positions;

function init(el, sock) {
  el.z0.classList.remove('hide');
  el.main.classList.add('hide');

  initSocket(sock);

  // renderer
  renderer = new THREE.WebGLRenderer();
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setSize(window.innerWidth, window.innerHeight);
  document.body.appendChild(renderer.domElement);

  // scene
  scene = new THREE.Scene();

  // camera
  camera = new THREE.PerspectiveCamera(
    45,
    window.innerWidth / window.innerHeight,
    1,
    10000,
  );
  camera.position.set(0, 0, 1000);

  // geometry
  var geometry = new THREE.BufferGeometry();

  // attributes
  positions = new Float32Array(MAX_POINTS * 3); // 3 vertices per point
  geometry.setAttribute('position', new THREE.BufferAttribute(positions, 3));

  // drawcalls
  drawCount = 2; // draw the first 2 points, only
  geometry.setDrawRange(0, drawCount);

  // material
  var material = new THREE.LineBasicMaterial({ color: 0xffffff, linewidth: 2 });

  // line
  line = new THREE.Line(geometry, material);
  scene.add(line);

  // update positions
  updatePositions();

  animate();
}

function initSocket(socket) {
  socket.emit('hello', 'Hello from client scene 0');
  socket.on('key', (keycode) => {
    console.log(`Receive key: ${keycode}`);
    positions.push(drawCount, keycode / 100, 0);
    drawCount = (drawCount + 1) % MAX_POINTS;
    line.geometry.setDrawRange(0, drawCount);
  });
}

// update positions
function updatePositions() {
  var positions = line.geometry.attributes.position.array;

  var x = 0,
    y = 0,
    z = 0,
    index = 0;

  for (var i = 0, l = MAX_POINTS; i < l; i++) {
    positions[index++] = x;
    positions[index++] = y;
    positions[index++] = z;

    x += (Math.random() - 0.5) * 30;
    y += (Math.random() - 0.5) * 30;
    z += (Math.random() - 0.5) * 30;
  }
}

// render
function render() {
  renderer.render(scene, camera);
}

// animate
function animate() {
  requestAnimationFrame(animate);

  /*
  if (drawCount === 0) {
    // periodically, generate new data
    updatePositions();
    line.geometry.attributes.position.needsUpdate = true; // required after the first render
    line.material.color.setHSL(Math.random(), 1, 0.5);
  }
  */
  render();
}

export { init };
