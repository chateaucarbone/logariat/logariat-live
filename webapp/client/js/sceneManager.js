// Scene Manager
//import * as scene0 from './scenes/scene0.js';
import * as scene1 from './scenes/scene1.js';

let elements = {
  z0: document.getElementById('z0'),
  //introCanvas: document.getElementById('intro_canvas'),
  main: document.querySelector('main'),
  z1: document.getElementById('z1'),
  z2: document.getElementById('z2'),
  z3: document.getElementById('z3'),
};

let currentScene,
  previousScene = 0;

//const scenes = [scene0];

function init(socket) {
  currentScene = 0;
  //scenes[currentScene].init(elements);
  scene1.init(elements, socket);
  //window.addEventListener('resize', scene0.resize);
}

function next() {
  // previousScene = currentScene;
  // currentScene++;
}

function prev() {}

// Exports
export { init };
