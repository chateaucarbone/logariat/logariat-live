//import { elements } from './js/sockets.js';
//import {default as randomSquare} from './modules/square.mjs';
// default  import randomSquare from './modules/square.mjs';
import * as sceneManager from './js/sceneManager.js';
//import * as sockets from './js/sockets.js';

// sockets
const socket = io();

//const comm = sockets.init();

//Scenes
sceneManager.init(socket);
